(defun write-big-endian (num filepath)

  (with-open-file (stream filepath
                          :direction :output
                          :element-type '(unsigned-byte 8)
                          :if-exists :supersede)
    (write-byte (ldb (byte 8 24) num) stream)
    (write-byte (ldb (byte 8 16) num) stream)
    (write-byte (ldb (byte 8 8) num) stream)
    (write-byte (ldb (byte 8 0) num) stream)))

(defun write-big-endian-s (num stream)
    (write-byte (ldb (byte 8 24) num) stream)
    (write-byte (ldb (byte 8 16) num) stream)
    (write-byte (ldb (byte 8 8) num) stream)
    (write-byte (ldb (byte 8 0) num) stream))

(defun write-short-big-endian-s (short stream)
    (write-byte (ldb (byte 8 8) short) stream)
    (write-byte (ldb (byte 8 0) short) stream))


(defun write-mthd (s midiformat ntrk division)
    (write-sequence (list (char-code #\M) (char-code #\T) (char-code #\h) (char-code #\d)) s)
    (write-big-endian-s 6 s)
    (write-short-big-endian-s midiformat s)
    (write-short-big-endian-s ntrk s)
    (write-short-big-endian-s division s))

(with-open-file (stream "test-hd"
                          :direction :output
                          :element-type '(unsigned-byte 8)
                          :if-exists :supersede)

    (write-mthd stream 1 7 120))
