(defclass midi-event ()
    ((delta-time 
        :initarg :delta-time
        :initform nil
        :accessor delta-time)
    (status
        :initform 0
        :accessor status)

    (pause
        :initform nil
        :accessor pause
        )
    )

)


(defclass channel-event (midi-event)
    (
    (delta-time 
        :initarg :delta-time
        :accessor delta-time)
    (status
        :initform #xb0
        :initarg :status
        :accessor status)
    (msg
        :initarg :msg
        :accessor msg)

    )
)


(defclass pause-event (midi-event)
    ((pause
        :initform t
        :accessor pause)))

(defclass note-on (channel-event)
    ((note        
        :initarg :note
        :accessor note
    )
    (vel
        :initarg :vel
        :accessor vel
    )
    (msg
        :initform #x90
    )
))


(defclass note-off (channel-event)
    ((note        
        :initarg :note
        :accessor note
    )
    (vel
        :initform 0
        :accessor vel
    )
    (msg
        :initform #x80
    )
))


(defgeneric ewrite (event) 
    (:documentation "event")
    (:method (obj) (format t "Gen"))
)

(defmethod initialize-instance :after ((obj channel-event) &key)
    (format t "EI type ~A " (type-of obj))
    (with-slots (status msg) obj
        (assert (and (>= status 0) msg) )))

(defmethod initialize-instance :after ((obj midi-event) &key)
    (format t "EI type ~A " (type-of obj))
    (with-slots (delta-time) obj
        (assert delta-time )))


(defmethod ewrite ((obj midi-event) )
    (format t "ewme type ~A " (type-of obj)))

(defmethod ewrite :before ((obj midi-event) )
    (format t "ewme before ~A " (type-of obj)))

(defmethod ewrite ((obj channel-event) )
    (format t "ewce type ~A msg ~A " (type-of obj) (msg obj)))

(defmethod ewrite ((obj pause-event) )
    (format t "ewp type ~A " (type-of obj)))


(defparameter ce (make-instance 'midi-event :delta-time 0))

(ewrite ce)
(format t "pause ~A status ~A~&" (status ce) (pause ce) )

(defparameter ce (make-instance 'channel-event :delta-time 0 :msg #x44 :status #xee))
(ewrite ce)
(format t "status ~A msg ~A pause ~A~&" (status ce) (msg ce) (pause ce) )


(defparameter ce (make-instance 'pause-event :delta-time 10))
(ewrite ce)
(format t "status ~A pause ~A~&" (status ce) (pause ce) )

(defparameter ce (make-instance 'note-on :delta-time 10 :note 91 :vel 100))
(ewrite ce)
(format t "status ~A dt ~A note ~A vel ~A msg ~A~&" (status ce) (delta-time ce) (note ce) (vel ce) (msg ce))

(defparameter ce (make-instance 'note-off :delta-time 10 :note 91))
(ewrite ce)
(format t "status ~A note ~A vel ~A msg ~A~&" (status ce)  (note ce) (vel ce) (msg ce))


(exit)

