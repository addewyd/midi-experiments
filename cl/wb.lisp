(with-open-file (s "temp-bytes" 
                    :direction :output
                    :element-type 'unsigned-byte)
    (write-byte 101 s))


(with-open-file (s "temp-s" 
                    :direction :output
                    :element-type 'unsigned-byte)
    (write-sequence '(100 101 102 104 120 42) s :end 5))

