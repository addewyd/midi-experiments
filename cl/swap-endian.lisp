(defun swap-endianness (num)
  "����������� 4-������� ����� �� little-endian � big-endian."
  (logior (ash (logand num #xff) 24)
          (ash (logand (ash num -8) #xff) 16)
          (ash (logand (ash num -16) #xff) 8)
          (logand (ash num -24) #xff)))
