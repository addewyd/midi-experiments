(ql:quickload :alexandria)

(defun read-project (filename)
    (with-open-file (in filename)
        (read in)
))


(let ((p (read-project "project.txl")))    
    (format t "P: ~A~&" p)
)

(let ((p  (uiop:safe-read-file-form "project.txl")))
    (format t "U: ~A~&" p)
    (let ((ht 
            (alexandria:alist-hash-table (cdr p))))
        (format t "U: ~A~&" ht)
    )
)
