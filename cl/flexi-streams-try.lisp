(ql:quickload "flexi-streams")

(defun write-bytes-to-array-with-stream (bytes)
  (let ((stream (flexi-streams:make-in-memory-output-stream)))
    (dolist (byte bytes)
      (write-byte byte stream))
    (flexi-streams:get-output-stream-sequence stream)))

(defun open-fstr ()
    (flexi-streams:make-in-memory-output-stream))

(defun write-fstr (s bytes)
    (dolist (byte bytes)
      (write-byte byte s))
    s)

(defun close-fstr (s)
    (close s))


(defvar bytes (list 33 66 55 45 67 89 120 200))

(let ((s (open-fstr)))
    (write-fstr s bytes)

    (let ((x (flexi-streams:get-output-stream-sequence s)))
        (with-open-file (stream "test-fs"
                          :direction :output
                          :element-type '(unsigned-byte 8)
                          :if-exists :supersede)
            (format t "~A~&"    x)
            (write-sequence x stream)))

    (close s))

