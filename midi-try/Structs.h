#pragma once
#include <iostream>

struct MThd {
    uint16_t format;
    uint16_t ntrk;
    uint16_t division;
    void write(std::stringstream& st) {
        st << "MThd";
        uint32_t length1 = le2beI(6);
        uint16_t format1 = le2beS(format);
        uint16_t ntrk1 = le2beS(ntrk);
        uint16_t division1 = le2beS(division);
        st.write(reinterpret_cast<const char*>(&length1), sizeof(length1));
        st.write(reinterpret_cast<const char*>(&format1), sizeof(format1));
        st.write(reinterpret_cast<const char*>(&ntrk1), sizeof(ntrk1));
        st.write(reinterpret_cast<const char*>(&division1), sizeof(division1));
    }
};

struct MTrk {
    uint32_t length;
    void write(std::stringstream& st) {
        st << "MTrk";
        uint32_t length1 = le2beI(length);
        st.write(reinterpret_cast<const char*>(&length1), sizeof(length1));
    }
};

