#include "Gen02.h"

std::ostream& operator<<(std::ostream& os, const MidiEvent& me) {
	os << int(me.status);
	return os;
}
int ecnt = 0;
uint16_t LEN0 = 120;

// ........................................................................................

int cnote(char nn, char mod, int alt) { // letter, length sign, -1/0/1
	int n = 0;
	if (nn == 'p') return -1;
	switch (nn) {
	case 'C': n = 60; break;
	case 'D': n = 62; break;
	case 'E': n = 64; break;
	case 'F': n = 65; break;
	case 'G': n = 67; break;
	case 'A': n = 69; break;
	case 'B': n = 71; break;
	case 'c': n = 72; break;
	case 'd': n = 74; break;
	case 'e': n = 76; break;
	case 'f': n = 77; break;
	case 'g': n = 79; break;
	case 'a': n = 81; break;
	case 'b': n = 83; break;
	default:  return 0;
	}
	n -= 0;
	switch (mod) {
	case '.': n -= 24; break;
	case ',': n -= 12; break;
	case '\'': n += 12; break;
	case '"': n += 24; break;
	default: break;
	}n += alt; 
	return n;
}

// ............................................................................................................

void TrackV2::parse_event(std::string& s, std::vector< std::shared_ptr<MidiEvent> > & vev) {
	std::vector<int> ch_notes;
	ecnt++;

	std::regex r("^([\\^_]?)([@A-GMPTVa-gimpqtwX])([\"',.]?)(\\/|#?)(\\d*)(\\*?)((v\\d+)*)(\\[(\\d+(=\\d+)*)\\])?$");
	// ((\[\d+(\=\d+)*\])?)
	const std::vector<std::string> alts{
		std::sregex_token_iterator { cbegin(s), cend(s), r, 1},
		std::sregex_token_iterator{}
	};
	const std::vector<std::string> name{
		std::sregex_token_iterator { cbegin(s), cend(s), r, 2},
		std::sregex_token_iterator{}
	};
	const std::vector<std::string> mod{
		std::sregex_token_iterator { cbegin(s), cend(s), r, 3},
		std::sregex_token_iterator{}
	};
	const std::vector<std::string> slash{
		std::sregex_token_iterator { cbegin(s), cend(s), r, 4},
		std::sregex_token_iterator{}
	};
	const std::vector<std::string> lens{
		std::sregex_token_iterator { cbegin(s), cend(s), r, 5},
		std::sregex_token_iterator{}
	};
	const std::vector<std::string> accs{
	std::sregex_token_iterator { cbegin(s), cend(s), r, 6},
	std::sregex_token_iterator{}
	};

	const std::vector<std::string> vels {
	std::sregex_token_iterator { cbegin(s), cend(s), r, 7},
	std::sregex_token_iterator{}
	};

	const std::vector<std::string> vels_in{
	std::sregex_token_iterator { cbegin(s), cend(s), r, 8},
	std::sregex_token_iterator{}
	};
	const std::vector<std::string> chord3{
	std::sregex_token_iterator { cbegin(s), cend(s), r, 10},
	std::sregex_token_iterator{}
	};

	uint32_t dt = 0;
	if (name.size() > 0) {
		//std::cout << name[0] << " " << mod[0] << slash[0] << lens[0] << " "; // std::endl;
		auto m = name[0];
		char N = m[0];
		//std::cout << "NNN " << m[0] << std::endl;
		if (N == 'i') {
			int patch = lens[0].length() > 0 ? (atoi(lens[0].c_str())) : 0;
			//track.ch_program(0, patch, tracknum);
			vev.push_back(std::make_shared<ChProgram>(0, patch));
			return;
		}
		if (N == 'q') {
			int Q = lens[0].length() > 0 ? (atoi(lens[0].c_str())) : 0;
			if (Q) {
				//track.set_tempo(Q);
				vev.push_back(std::make_shared<Tempo>(0, Q));
			}
			return;
		}

		if (N == 'w') { // pitch wheel
			int pitch = lens[0].length() > 0 ? (atoi(lens[0].c_str())) : 0;
			vev.push_back(std::make_shared<PitchWheel>(0, pitch));  // center 8192 0x3fff  max 16383
			return;
		}

		if (N == 'M') {
			int val = lens[0].length() > 0 ? (atoi(lens[0].c_str())) : 0;
			vev.push_back(std::make_shared<ModulationC>(0, val));
			return;
		}

		if (N == 'm') { // Not used
			int val = lens[0].length() > 0 ? (atoi(lens[0].c_str())) : 0;
			vev.push_back(std::make_shared<ModulationF>(0, val));
			return;
		}

		if (N == 'P') { //Pan
			int val = lens[0].length() > 0 ? (atoi(lens[0].c_str())) : 0;
			vev.push_back(std::make_shared<Pan>(0, val));
			return;
		}

		if (N == '@') { // Bank select lsb
			int val = lens[0].length() > 0 ? (atoi(lens[0].c_str())) : 0;
			if (val > 127) {
				//nah
				vev.push_back(std::make_shared<BankSelect>(0, val & 0xff));
				vev.push_back(std::make_shared <BankSelect32>(0, val >> 8));
			}
			else {
				vev.push_back(std::make_shared <BankSelect>(0, val));
				vev.push_back(std::make_shared <BankSelect32>(0, 0));
			}
			return;
		}

		if (N == 'T' || N == 't') { // transpose track
			int val = lens[0].length() > 0 ? (atoi(lens[0].c_str())) : 0;
			if (N == 't') {
				shift += val;
			}
			else {
				shift -= val;
			}
			return;
		}

		if (N == 'X') { // Expr
			int val = lens[0].length() > 0 ? (atoi(lens[0].c_str())) : 0;
			vev.push_back(std::make_shared <Expression>(0, val));
			//auto e = std::make_unique<Expression>(0, val);
			//vev.push_back(*e`);
			return;
		}
		if (N == 'V') { // Volume
			int val = lens[0].length() > 0 ? (atoi(lens[0].c_str())) : 0;
			vev.push_back(std::make_shared <Volume>(0, val));
			return;
		}
		ch_notes.clear();
		if (chord3[0].length() > 0) {
			//std::cout << "chord3 " << chord3[0] << std::endl;
			std::regex rgx("=");
			std::sregex_token_iterator iter(chord3[0].begin(),
				chord3[0].end(),
				rgx,
				-1);
			std::sregex_token_iterator end;
			for (; iter != end; ++iter) {
				auto s = (*iter).str();
				//std::cout << "CH  " << s << std::endl;
				ch_notes.push_back(atoi(s.c_str()));
			}
		}

		int alt = alts[0].length() > 0 ? (alts[0][0] == '_' ? -1 : 1) : 0;
		bool sl = (slash[0].length() > 0) && (slash[0][0] == '/');
		bool abs = (slash[0].length() > 0) && (slash[0][0] == '#');
		int fact = lens[0].length() > 0 ? (atoi(lens[0].c_str())) : 0;
		bool acc = accs[0].length() > 0;

		int vel = 64;
		if (vels.size() > 0) {
			if (vels[0].length() > 1) {
				vel = atoi(vels[0].substr(1).c_str());
			}
		}

		int len0 = LEN0;
		if (fact) {
			if (sl) {
				len0 /= fact;
			}
			else {
				if (abs) len0 = fact;
				else len0 *= fact;
			}
		}

		char cmod = mod[0].length() > 0 ? mod[0][0] : '\0';
		int note = cnote(N, cmod, alt);
		if (note == -1) { // pause
			vev.push_back(std::make_shared <Pause>(len0/* /2 */));  // ???????
		}
		else {
			note += shift;
			if (note > 0 && note <= 127) {
				if (acc) {
					vel += 32;
					if (vel > 127) vel = 127;

				}
				vev.push_back(std::make_shared <NoteOn>(0, note, vel));
				for (size_t i = 0; i < ch_notes.size(); i++) {
					vev.push_back(std::make_shared <NoteOn>(0, note + ch_notes[i], vel));
				}			
				vev.push_back(std::make_shared <NoteOff>(len0, note));
				for (size_t i = 0; i < ch_notes.size(); i++) {
					vev.push_back(std::make_shared <NoteOff>(0, note + ch_notes[i]));
				}
			}
		}
	}
	else {
		// other events
		std::regex r2("K(-?\\d)\\|([01])"); // sf|mi
		std::regex r3("TS(\\d+)\\|(\\d+)\\|(\\d+)\\|(\\d+)"); // sf|mi
		const std::vector<std::string> ssf {
			std::sregex_token_iterator { cbegin(s), cend(s), r2, 1},
			std::sregex_token_iterator{}
		};
		const std::vector<std::string> smi{
			std::sregex_token_iterator { cbegin(s), cend(s), r2, 2},
			std::sregex_token_iterator{}
		};
		// .......................
		const std::vector<std::string> ts1 {
			std::sregex_token_iterator { cbegin(s), cend(s), r3, 1},
			std::sregex_token_iterator{}
		};
		const std::vector<std::string> ts2{
			std::sregex_token_iterator { cbegin(s), cend(s), r3, 2},
			std::sregex_token_iterator{}
		};
		const std::vector<std::string> ts3{
			std::sregex_token_iterator { cbegin(s), cend(s), r3, 3},
			std::sregex_token_iterator{}
		};
		const std::vector<std::string> ts4{
			std::sregex_token_iterator { cbegin(s), cend(s), r3, 4},
			std::sregex_token_iterator{}
		};
		if (ssf.size() > 0) {
			int sf = ssf[0].length() > 0 ? (atoi(ssf[0].c_str())) : 0;
			int mi = smi[0].length() > 0 ? (atoi(smi[0].c_str())) : 0;
			vev.push_back(std::make_shared <Key>(0, sf, mi));
			return;
		}

		if (ts1.size() > 0) {
			int d1 = ts1[0].length() > 0 ? (atoi(ts1[0].c_str())) : 0;
			int d2 = ts2[0].length() > 0 ? (atoi(ts2[0].c_str())) : 0;
			int d3 = ts3[0].length() > 0 ? (atoi(ts3[0].c_str())) : 0;
			int d4 = ts4[0].length() > 0 ? (atoi(ts4[0].c_str())) : 0;
			vev.push_back(std::make_shared <TimeSign>(0, d1, d2, d3, d4));
			return;
		}
	}
}

// .....................................................................................................

std::vector< std::shared_ptr<TrackV2> >* Generate(std::list<std::string>& lines, std::string o) {
	int track_counter = 0;
	std::stringstream f;
	uint16_t ntrk = 1;
	MThd mthd = { 1, ntrk, LEN0 }; //
	mthd.write(f);
	std::vector< std::shared_ptr<TrackV2> >* tracks = new std::vector< std::shared_ptr<TrackV2> >();
	std::vector< std::shared_ptr<MidiEvent> > vev;

	std::shared_ptr <TrackV2> pt = 0;
	Voice* pv = 0;
	int lc = 0;
	int channel = 0;
	for (auto const& line : lines) {
		lc++;

		if (line.size() > 1 && line[0] == '/' && line[1] == '/') { // comment
			lc++;
			continue;
		}

		std::regex e("^Track\\s+Ch\\s+(\\d+)\\s+(.*)"); // channel num trackname
		std::sregex_token_iterator c(line.begin(), line.end(), e, { 1, 2 });
		// default constructor = end-of-sequence:
		std::sregex_token_iterator end;
		if (c != end) {
			std::vector<std::string> tr;
			// new track
			copy(c, end, std::back_inserter(tr));
			//std::cout << tr[0] << " " << tr[1] << std::endl;
			channel = atoi(tr[0].c_str());
			if (pt) {
			//	pt->eot();
				pt->write();
				//delete pt;
				pt = 0;
			}
			track_counter++;
			pt = std::make_shared<TrackV2>(&f, channel);
			pt->name(tr[1]);
// -------------------------------------------------------
// adding to tracks
			tracks->push_back(pt);
			continue;
		}
		std::regex e2("^Voice");
		std::cmatch m;
		if (std::regex_search(line.c_str(), m, e2)) {
			pv = new Voice(channel);
			if(!pt) {
				std::cout << "Error voice without track in line " << lc << std::endl;
				break;
			}
			pt->AddVoice(pv);
			continue;
		}

		if (pv) {
			std::regex rgx("\\s+");
			std::sregex_token_iterator iter(line.begin(),
				line.end(),
				rgx,
				-1);
			std::sregex_token_iterator end;
			for (; iter != end; ++iter) {
				auto s = (*iter).str();
				if (s.length() > 0) {
					// event str represent
					vev.clear();
					pt -> parse_event(s, vev);
					for (size_t i = 0; i < vev.size(); i++) {
						pv->AddEvent(vev[i]);
					}
				}
			}
		}	
	}

	if (pt) {
		//pt->eot();
		pt->write();
		//delete pt;
	}

	f.seekp(10);
	uint16_t nt = le2beS(uint16_t(track_counter));
	f.write(reinterpret_cast<const char*>(&nt), sizeof(nt));
	f.seekp(0, std::ios::end);

	// ..................................................................

	std::ofstream file;

	file.open(o.c_str(), std::ios::binary);
//	std::cout << o << std::endl;
	file << f.str();
	file.close();
	return tracks;
}