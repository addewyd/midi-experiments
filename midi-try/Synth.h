#pragma once

#include <fluidsynth.h>
#include <vector>

// ..........................................................

class Synth {
	unsigned int time_marker;
	short dest;
	fluid_synth_t* synth;
	fluid_audio_driver_t* audiodriver;
	fluid_settings_t* settings;
	int channel;
public:
	Synth(int c, std::string& sf2) : channel(c), time_marker(0), dest(0) {
		settings = new_fluid_settings();
		
//		fluid_settings_setint(settings, "synth.chorus.active", false);
		synth = new_fluid_synth(settings);
		int n = fluid_synth_sfload(synth, sf2.c_str(), 1);
		audiodriver = new_fluid_audio_driver(settings, synth);
		
	}
	fluid_synth_t* GetSynth() {
		return synth;
	}
	short GetDest() {
		return dest;
	}
	int GetChannel() {
		return channel;
	}
	void SetDest(short d) {
		dest = d;
	}
	void SetMarker(unsigned int m) {
		time_marker = m;
	}
};

// ..........................................................

class Sequencer {
	const int DTF = 4;  // 1440 / 4 = 360 ??
	fluid_sequencer_t* sequencer;
	//std::vector<short> synth_destinations;
	int time_marker;
	std::vector<std::shared_ptr<Synth> > synths;
	std::vector< std::shared_ptr<TrackV2> > *tracks;
	std::string sf2;
public:

	Sequencer(std::vector<std::shared_ptr<TrackV2> > *_t, const char *_sf2) {
		tracks = _t;
		sf2 = std::string(_sf2);
		sequencer = new_fluid_sequencer2(0);
		for (size_t it = 0; it < tracks->size(); it++) {
			TrackV2& t = *(tracks->at(it));
			int channel = t.GetChannel();
			std::shared_ptr <Synth> sy = std::make_shared<Synth>(channel, sf2);
			short sd = fluid_sequencer_register_fluidsynth(sequencer, sy->GetSynth() );
			AddSynth(sy);
		}
	}

	void AddSynth(std::shared_ptr <Synth> s) {
		short synth_destination = fluid_sequencer_register_fluidsynth(sequencer, s -> GetSynth());
		synths.push_back(s);
		unsigned int time_marker = fluid_sequencer_get_tick(sequencer);
		s->SetMarker(time_marker);
		s->SetDest(synth_destination);
	}

	// ..................................................................

	void Schedule() {
		bool ok = true;

		// sequencer start time (ticks)
		time_marker = fluid_sequencer_get_tick(sequencer);

		std::vector<int> indices;
		for (size_t it = 0; it < tracks->size(); it++) {
			indices.push_back(0);
			std::cout << "Name " << tracks->at(it)->Name << " Channel " << tracks->at(it)->GetChannel() << std::endl;
		}
		while (ok) {
			ok = false;
			for (size_t it = 0; it < tracks->size(); it++) {
				size_t e_index = indices[it];
				if (e_index < 0) {
					continue;
				}
				auto evs = tracks->at(it)->TrackEvents;
				size_t e_size = evs.size();
				if (e_index >= e_size) {
					indices[it] = -1;
					continue;
				}
				schedule_event(evs[e_index], synths[it]/*synths[it]->GetDest(), synths[it]->GetChannel()*/);
				e_index++;
				indices[it] = e_index;
				ok = true;
			}
			if (!ok) break;
		}
	}
private:

	void noteon(int chan, short key, unsigned int ticks, short dest)
	{
		//std::cout << "ON  " << ticks << " C " << chan << std::endl;
		fluid_event_t* ev = new_fluid_event();
		fluid_event_set_source(ev, -1);
		fluid_event_set_dest(ev, dest);
		fluid_event_noteon(ev, chan, key, 127);
		fluid_sequencer_send_at(sequencer, ev, ticks, 1);
		delete_fluid_event(ev);
	}

	void noteoff(int chan, short key, unsigned int ticks, short dest)
	{
		//std::cout << "OFF " << ticks << " C " << chan << std::endl;
		fluid_event_t* ev = new_fluid_event();
		fluid_event_set_source(ev, -1);
		fluid_event_set_dest(ev, dest);
		fluid_event_noteoff(ev, chan, key);
		fluid_sequencer_send_at(sequencer, ev, ticks, 1);
		delete_fluid_event(ev);
	}

	void chprogram(int chan, int val, unsigned int ticks, short dest)
	{
		//std::cout << "OFF " << ticks << " C " << chan << std::endl;
		fluid_event_t* ev = new_fluid_event();
		fluid_event_set_source(ev, -1);
		fluid_event_set_dest(ev, dest);
		fluid_event_program_change(ev, chan, val);
		fluid_sequencer_send_at(sequencer, ev, ticks, 1);
		delete_fluid_event(ev);
	}

	void pitchwheel(int chan, int v1, int v2, unsigned int ticks, short dest)
	{
		fluid_event_t* ev = new_fluid_event();
		fluid_event_set_source(ev, -1);
		fluid_event_set_dest(ev, dest);
		int val = (v2 << 7) | v1;
		//std::cout << "PW " << ticks << " v " << val << std::endl;
		fluid_event_pitch_bend(ev, chan, val);
		fluid_sequencer_send_at(sequencer, ev, ticks, 1);
		delete_fluid_event(ev);
	}

	void controller(int chan, int cntr, int val, unsigned int ticks, short dest)
	{
		fluid_event_t* ev = new_fluid_event();
		fluid_event_set_source(ev, -1);
		fluid_event_set_dest(ev, dest);
		fluid_event_control_change(ev, chan, cntr, val);
		fluid_sequencer_send_at(sequencer, ev, ticks, 1);
		delete_fluid_event(ev);
	}

	void bank(int chan, int val, unsigned int ticks, short dest)
	{
		fluid_event_t* ev = new_fluid_event();
		fluid_event_set_source(ev, -1);
		fluid_event_set_dest(ev, dest);
		fluid_event_bank_select(ev, chan, val);
		fluid_sequencer_send_at(sequencer, ev, ticks, 1);
		delete_fluid_event(ev);
	}

	// ..............................................

	void schedule_event(std::shared_ptr<MidiEvent> ev, std::shared_ptr <Synth> synth) {

		short dest = synth->GetDest();
		int chan = synth->GetChannel();
		fluid_synth_t* sy = synth->GetSynth();
		//std::cout << "EV " << int(ev->status) << " T " << int(ev->abs_time) << " D " << dest << std::endl;
		if ((ev->status & 0xf0) == 0xC0) {
			//fluid_synth_program_change(sy, chan, ev->data[0]);
			int dt = ev->abs_time;
			int note_time = dt * DTF + time_marker;
			chprogram(chan, ev->data[0], note_time, dest);
		}
		else if ((ev->status & 0xf0) == 0xe0) {
			
			int dt = ev->abs_time;
			int note_time = dt * DTF + time_marker;

			pitchwheel(chan, ev->data[0], ev->data[1], note_time, dest);
		}
		else if ((ev->status & 0xf0) == 0xb0) { // controller

			int dt = ev->abs_time;
			int note_time = dt * DTF + time_marker;
			if (ev->data[0] == 0x0) {
				bank(chan, ev->data[1], note_time, dest);
			}
			else {
				controller(chan, ev->data[0], ev->data[1], note_time, dest);
			}
		}
		else if ((ev->status & 0xf0) == 0x90) {
			//schedule_timer_event();
			int dt = ev->abs_time;
			int note_time = dt * DTF + time_marker;
			//int ch = (ev->status & 0x0f);
			noteon(chan, ev->data[0], note_time, dest);
//			time_marker += dt;
		}
		else if ((ev->status & 0xf0) == 0x80) {
			//schedule_timer_event();
			int dt = ev->abs_time;
			int note_time = dt * DTF + time_marker;
			//int ch = (ev->status & 0x0f);
			noteoff(chan, ev->data[0], note_time, dest);
//			time_marker += dt;
		}
	}
};

