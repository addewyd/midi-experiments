#include "Utils.h"

uint32_t le2beI(uint32_t num) {
    uint32_t b0, b1, b2, b3;

    b0 = (num & 0x000000ff) << 24u;
    b1 = (num & 0x0000ff00) << 8u;
    b2 = (num & 0x00ff0000) >> 8u;
    b3 = (num & 0xff000000) >> 24u;

    return b0 | b1 | b2 | b3;
}

uint16_t le2beS(uint16_t num) {
    uint16_t b1, b2;

    b1 = (num & 0x00ff) << 8u;
    b2 = (num & 0xff00) >> 8u;

    return b1 | b2;
}

void write_varlen(uint32_t value, std::stringstream& f)
{
    uint32_t buffer;
    buffer = value & 0x7f;
    while ((value >>= 7) > 0)
    {
        buffer <<= 8;
        buffer |= 0x80;
        buffer += (value & 0x7f);
    }
    while (1)
    {
        uint8_t b = buffer & 0xff;
        f.write(reinterpret_cast<const char*>(&b), sizeof(b));
        if (buffer & 0x80) buffer >>= 8;
        else
            break;
    }
}

