#pragma once
#include <stdint.h>
#include <fstream>
#include <sstream>

uint32_t le2beI(uint32_t);
uint16_t le2beS(uint16_t);
void write_varlen(uint32_t, std::stringstream&);