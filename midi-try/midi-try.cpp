﻿#include "Gen02.h"
#include "Synth.h"
#include <filesystem>
#include <Windows.h>

void Play(std::vector< std::shared_ptr<TrackV2> > * tracks, const char *sf2) {
    Sequencer S(tracks, sf2);
    S.Schedule();
    return;   
}

std::filesystem::path abs_exe_path()
{
#if defined(_MSC_VER)
    wchar_t path[FILENAME_MAX] = { 0 };
    GetModuleFileNameW(nullptr, path, FILENAME_MAX);
    return std::filesystem::path(path);
#else
    char path[FILENAME_MAX];
    ssize_t count = readlink("/proc/self/exe", path, FILENAME_MAX);
    return std::filesystem::path(std::string(path, (count > 0) ? count : 0));
#endif
}

std::filesystem::path abs_exe_directory()
{
#if defined(_MSC_VER)
    wchar_t path[FILENAME_MAX] = { 0 };
    GetModuleFileNameW(nullptr, path, FILENAME_MAX);
    return std::filesystem::path(path).parent_path().string();
#else
    char path[FILENAME_MAX];
    ssize_t count = readlink("/proc/self/exe", path, FILENAME_MAX);
    return std::filesystem::path(std::string(path, (count > 0) ? count : 0)).parent_path().string();
#endif
}

int main(int argc, char *argv[])
{
    std::cout << "txm parser/midi generator\n";
    if (argc > 1) {
        std::ifstream in(argv[1]);
        std::string s;
        std::list<std::string> lines;
        while (getline(in, s)) {
            lines.push_back(s);
        }
        std::string o(argv[1]);
            std::cout << "Ver2\n";
            o = o + "V2.mid";
            std::vector< std::shared_ptr<TrackV2> >* tracks = Generate(lines, o);
            if (true) {
                std::filesystem::path sff = abs_exe_directory() += "\\GeneralUser GS MuseScore v1.442.sf2";
                // "C:\\Users\\user\\Documents\\MuseScore3\\SoundFonts\\Arachno SoundFont - Version 1.0.sf2"
                // "C:\\Users\\user\\Music\\SF2\\Timbres of Heaven (XGM) 4.00(G)\\Timbres of Heaven (XGM) 4.00(G).sf2"
                // "C:\\wprfl\\fluidsynth\\sf2\\VintageDreamsWaves-v2.sf2"
                Play(tracks, sff.string().c_str());
                std::cout << "press <Enter> to stop\n";
                int n = getchar();

            }
        return 0;
    }

    return -1;
}

