#pragma once
//#include "Synth.h"
#include "Utils.h"

#include "Structs.h"

#include <string>
#include <list>
#include <vector>
#include <array>
#include <regex>
#include <iostream>
#include <algorithm>

int cnote(char nn, char mod, int alt);

struct MidiEvent {

	uint32_t delta_time;
	uint32_t abs_time;
	uint8_t status;
	uint8_t channel;
	uint8_t length;
	std::array<uint8_t, 4> data = { {0,0,0,0} };
	bool pause;
	MidiEvent(uint32_t _delta_time)
		: delta_time(_delta_time), abs_time(0), status(0), length(0), data({0,0,0,0})
	{
		channel = 0;
		//abs_time = 0;
		pause = false;
	}

	virtual void write(std::stringstream&) = 0;
	friend std::ostream& operator<<(std::ostream& os, const MidiEvent& me);

	void w1b(uint8_t b, std::stringstream& s) {
		s.write(reinterpret_cast<const char*>(&b), 1);
	}
};

struct ChannelEvent : MidiEvent {
	ChannelEvent(uint32_t _delta_time, uint8_t msg, uint8_t v)
		: MidiEvent(_delta_time)
	{
		data[0] = msg;
		data[1] = v;
		status = 0xb0;  // controller message
	}

	// with another status
	ChannelEvent(uint32_t _delta_time, uint8_t _status) // for pitch wheel and sort of it
		: MidiEvent(_delta_time)
	{
		//delta_time = _delta_time;
		status = _status;
	}

	virtual void write(std::stringstream& s) {
		write_varlen(delta_time, s);
		w1b(status | channel, s);
		w1b(data[0], s);
		w1b(data[1], s);
	}
};

struct Pause : MidiEvent {
public:
	Pause(uint32_t _delta_time) : MidiEvent(_delta_time) {
		delta_time = _delta_time;
		pause = true;
	}
	virtual void write(std::stringstream& s) {}
};

struct NoteOn : ChannelEvent {
	NoteOn(uint32_t _delta_time, uint8_t note, uint8_t vel) 
		: ChannelEvent(_delta_time, 0x90)
	{
		data[0] = note;
		data[1] = vel;
	}
};

struct NoteOff : ChannelEvent {
	NoteOff(uint32_t _delta_time, uint8_t note)
		: ChannelEvent(_delta_time, 0x80)
	{
		data[0] = note;
		data[1] = 0;
	}
};

struct ChProgram : MidiEvent {
	ChProgram(uint32_t _delta_time, uint8_t _i)
		: MidiEvent(_delta_time)
	{
		//delta_time = _delta_time;
		status = 0xC0;
		data[0] = _i;
	}
	virtual void write(std::stringstream& s) {
		write_varlen(delta_time, s);
		w1b(status | channel, s);
		w1b(data[0], s);  /// i
	}

};

struct BankSelect : ChannelEvent {
	BankSelect(uint32_t _delta_time, uint8_t _i)
		: ChannelEvent(_delta_time, 0x0, _i)
	{}
};

struct BankSelect32 : ChannelEvent {
	BankSelect32(uint32_t _delta_time, uint8_t _i)
		: ChannelEvent(_delta_time, 0x20, _i)
	{}
};

struct Tempo : MidiEvent {  // -> sy
	uint16_t bpq;
	Tempo(uint32_t _delta_time, uint16_t _i)
		: MidiEvent(_delta_time)
	{
		//delta_time = _delta_time;
		status = 0xff;
		data[0] = 0x51;
		bpq = _i;
	}
	virtual void write(std::stringstream& s) {
		// 0xFF 0x51 0x03 0x07 0xA1 0x20 = 500000
		int bpm = 60000000 / bpq;
		int a = bpm; //le2beI(bpm);
		uint8_t len = 3;
		write_varlen(delta_time, s);
		w1b(status, s); w1b(data[0], s); w1b(len, s);

		uint8_t b = a >> 16;
		w1b(b, s);
		b = (a >> 8) & 255;
		w1b(b, s);
		b = a & 255;
		w1b(b, s);
	}
};

struct TimeSign : MidiEvent {   // -> sy
	uint8_t numerator;
	uint8_t denominator; uint8_t metr; uint8_t _32perbeat;

	TimeSign(uint32_t _delta_time, uint8_t _numerator, uint8_t _denominator, 
		uint8_t _metr, uint8_t __32perbeat)
		: MidiEvent(_delta_time)
	{
		//delta_time = _delta_time;
		status = 0xff;
		data[0] = 0x58;
		numerator = _numerator;
		denominator = _denominator;
		metr = _metr;
		_32perbeat = __32perbeat;
	}

	virtual void write(std::stringstream& s) {
		write_varlen(delta_time, s);		
		uint8_t len = 4;
		w1b(status, s); w1b(data[0], s); w1b(len, s);
		w1b(numerator, s);
		w1b(denominator, s);
		w1b(metr, s);
		w1b(_32perbeat, s);
	}
};

struct Key : MidiEvent {	  // -> sy
	char sf, mi;
	Key(uint32_t _delta_time, char _sf, char _mi) 
		: MidiEvent(_delta_time)
	{
		//delta_time = _delta_time;
		status = 0xff;
		data[0] = 0x59;
		sf = _sf;
		mi = _mi;
	}

	virtual void write(std::stringstream& s) {
		write_varlen(delta_time, s);
		uint8_t len = 2;
		w1b(status, s); w1b(data[0], s); w1b(len, s);
		w1b(sf, s);
		w1b(mi, s);
	}
};

struct PitchWheel : ChannelEvent {
	PitchWheel(uint32_t _delta_time, int16_t val)
		: ChannelEvent(_delta_time, 0xe0)
	{
		data[0] = (val & 0x7f);
		data[1] = ((val << 1) >> 8);

	}
};

struct ModulationC : ChannelEvent {
	ModulationC(uint32_t _delta_time, uint8_t v)
		: ChannelEvent(_delta_time, 0x1, v) {}
};

struct ModulationF : ChannelEvent { // Not used
	ModulationF(uint32_t _delta_time, uint8_t v)
		: ChannelEvent(_delta_time, 0x21, v) {}
};

struct Pan : ChannelEvent {
	Pan(uint32_t _delta_time, uint8_t v)
		: ChannelEvent(_delta_time, 0x0A, v) {}
};

struct Volume : ChannelEvent {
	Volume(uint32_t _delta_time, uint8_t v)
		: ChannelEvent(_delta_time, 0x07, v){}
};

struct Expression : ChannelEvent {
	Expression(uint32_t _delta_time, uint8_t v)
		: ChannelEvent(_delta_time, 0x0B, v) {}
};

class Voice {
private:
	std::vector<std::shared_ptr<MidiEvent> > events;
	uint32_t gentime;
	uint32_t last_dt;
	int channel;
public:

	// ..........................................................

	std::shared_ptr <MidiEvent> operator [] (int i) {
		return events[i];
	}

	// ..........................................................

	size_t size() { return events.size(); }

	// ..........................................................

	Voice(int ch) {
		gentime = 0;
		last_dt = 0;
		channel = ch;
	}

	// ..........................................................

	// ���-�� ���� � ������� -- fixed? Fix1
	void AddEvent(std::shared_ptr <MidiEvent> e) {
		e->channel = channel; // !!!!!! why here
		if (e->pause) {
			last_dt += e->delta_time;
		}
		else {
			e->delta_time += last_dt;
			//e->abs_time += e->delta_time;
			last_dt = 0;
			gentime += e->delta_time;
			e->abs_time = gentime;
			events.push_back(e);
			//std::cout << "AT " << int(e->status) << " " << e->abs_time << std::endl;
		}
	}

	// ..........................................................

	void print() {
		for (auto const& e : events) {
			std::cout << (*e) << std::endl;
		}
	}
};


// ..........................................................

class TrackV2 {
private:
	std::vector<Voice*> voices;
	std::stringstream* s;
	std::streamoff trackStartPos;
	std::streamoff trackLenPos;
	int channel;
	int shift;

public:

	// ..........................................................

	TrackV2(std::stringstream* _s, int ch) : shift(0) {
		s = _s;
		channel = ch;
		MTrk mtrk = { 0 };
		mtrk.write(*s);
		trackStartPos = (*s).tellp();
		trackLenPos = trackStartPos - 4;
	}

	std::vector<std::shared_ptr<MidiEvent> > TrackEvents;
	std::string Name;

	// ..........................................................

	void parse_event(std::string& s, std::vector< std::shared_ptr<MidiEvent> >& vev);

	// ..........................................................

	void AddVoice(Voice* v) {
		voices.push_back(v);
	}
	void SetShift(int s) {
		shift = s;
	}

	// ..........................................................

	int GetShift() {
		return shift;
	}

	// ..........................................................

	int GetChannel() {
		return channel;
	}

	// ..........................................................

	void name(std::string n) {
		Name = n;
		uint8_t len = uint8_t(n.length());
		write_varlen(0, *s);
		uint8_t status = 0xff;
		uint8_t cmd = 0x3;
		s->write(reinterpret_cast<const char*>(&status), 1);
		s->write(reinterpret_cast<const char*>(&cmd), 1);
		s->write(reinterpret_cast<const char*>(&len), 1); // string len
		s->write(n.c_str(), len);	
	}

	// ..........................................................

	void eot() {
		write_varlen(0, *s);
		uint8_t status = 0xff;
		uint8_t d1 = 0x2f;
		uint8_t d2 = 0x0;
		s->write(reinterpret_cast<const char*>(&status), sizeof(status));
		s->write(reinterpret_cast<const char*>(&d1), sizeof(d1));
		s->write(reinterpret_cast<const char*>(&d2), sizeof(d2));
		std::streamoff curPos = s->tellp();
		std::streamoff trackLen = curPos - trackStartPos;
		s->seekp(trackLenPos, std::ios::beg);
		uint32_t l = le2beI(uint32_t(trackLen));
		s->write(reinterpret_cast<const char*>(&l), sizeof(l));
		s->seekp(0, std::ios::end);
	}

	// ..........................................................

	void write_serial () {
		for (size_t i = 0; i < voices.size(); i++) {
			auto v = voices[i];
			for (size_t j = 0; j < v->size(); j++) {
				auto a = (*v)[j];
				a->write(*s);
			}
		}
		eot();
	}

	// ..........................................................

	void write() {
		bool ok = true;
		int ev_index = 0;
		int sumdt = 0;
		std::vector<size_t> indices;
		std::vector<uint32_t> abs_times;

		for (size_t i = 0; i < voices.size(); i++) {
			indices.push_back(0);
		}
		for (size_t i = 0; i < voices.size(); i++) {
			abs_times.push_back(0);
		}

		uint32_t last_written_time = 0;

		while (ok) {  // through voice
			//ok = true;

			for (size_t i = 0; i < voices.size(); i++) {
				auto v = *voices[i];
				if (v.size() > indices[i]) {
					abs_times[i] = v[indices[i]]->abs_time;					
				}
				else {
					abs_times[i] = UINT32_MAX;
				}
			}

			uint32_t min_abs_time = UINT32_MAX;
			int min_abs_time_voice_index = -1;
			for (size_t i = 0; i < voices.size(); i++) {
				auto v = *voices[i];
				if (v.size() > indices[i]) {
					//min_abs_time = std::min(min_abs_time, v[indices[i]]->abs_time);
					if (v[indices[i]]->abs_time < min_abs_time) {  // !!!! comparison
						min_abs_time = v[indices[i]]->abs_time;
						min_abs_time_voice_index = int(i);
					}
				}
			}
			// ............................................
			if (min_abs_time_voice_index > -1) {
				auto v = *voices[min_abs_time_voice_index];
				// recalculate delta_time here
				uint32_t cur = v[indices[min_abs_time_voice_index]]->abs_time;
				//std::cout <<" TT  "  << last_written_time << " " << cur << std::endl;
				std::shared_ptr <MidiEvent> event = v[indices[min_abs_time_voice_index]];

				/// <summary>
				///  ? Fix1
				/// </summary>
				event->delta_time = cur - last_written_time;
				last_written_time = cur;
				event->write(*s);

				//std::cout << "Event wr " << int(event->status) << " T " << int(event->abs_time) << std::endl;

				TrackEvents.push_back(event);
				//event->synth();
				indices[min_abs_time_voice_index] ++;
			}
			else {
				ok = false;
			}
			ev_index++;
		}
		eot();
	}	
};

// ................................................................................................

std::vector< std::shared_ptr<TrackV2> >* Generate(std::list<std::string>& lines, std::string o);
